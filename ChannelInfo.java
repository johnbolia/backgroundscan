package com.dsi.ant.sample.backgroundscan;

import com.dsi.ant.message.fromant.DataMessage;

public class ChannelInfo {

    private static final String TAG = "ChannelInfo";
    public final int deviceNumber;

    /** Master / Slave */
    public final boolean isMaster;

    public final int deviceType;
    public final int transmissionType;
    public final int period;  // 1/32768 of a second
    public final int offsetFrequency;  // offset frequency of 2.4Mhz

    public byte[] broadcastData = new byte[DataMessage.LENGTH_STANDARD_PAYLOAD];

    public boolean error;
    private String mErrorMessage;

    public ChannelInfo(int deviceNumber, boolean isMaster, int deviceType, int transmissionType,
                       int period, int offsetFrequency)
    {
        this.deviceNumber = deviceNumber;
        this.isMaster = isMaster;
        this.deviceType = deviceType;
        this.transmissionType = transmissionType;
        this.period = period;
        this.offsetFrequency = offsetFrequency;

//        // Not actually concerned with this value, so can cast to byte and lose data without issues
        broadcastData[0] = (byte)0;
        broadcastData[1] = (byte)0;
        broadcastData[2] = (byte)0;

        error = false;
        mErrorMessage = null;
    }

    public void die(String errorMessage)
    {
        error = true;
        mErrorMessage = errorMessage;
    }

    public String getErrorString()
    {
        return mErrorMessage;
    }

}

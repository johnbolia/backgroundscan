package com.dsi.ant.sample.backgroundscan;


import android.os.RemoteException;
import android.util.Log;

import com.dsi.ant.channel.AntChannel;
import com.dsi.ant.channel.AntCommandFailedException;
import com.dsi.ant.channel.BackgroundScanState;
import com.dsi.ant.channel.BurstState;
import com.dsi.ant.channel.EventBufferSettings;
import com.dsi.ant.channel.IAntAdapterEventHandler;
import com.dsi.ant.channel.IAntChannelEventHandler;
import com.dsi.ant.channel.UnsupportedFeatureException;
import com.dsi.ant.message.ChannelId;
import com.dsi.ant.message.ChannelType;
import com.dsi.ant.message.ExtendedAssignment;
import com.dsi.ant.message.LibConfig;
import com.dsi.ant.message.fromant.AcknowledgedDataMessage;
import com.dsi.ant.message.fromant.BroadcastDataMessage;
import com.dsi.ant.message.fromant.ChannelEventMessage;
import com.dsi.ant.message.fromant.MessageFromAntType;
import com.dsi.ant.message.ipc.AntMessageParcel;

public class ChannelController {

    private static final String TAG = ChannelController.class.getSimpleName();
    private AntChannel mAntChannel;
    private ChannelBroadcastListener mChannelBroadcastListener;

    private ChannelEventCallback mChannelEventCallback = new ChannelEventCallback();
    private ChannelInfo mChannelInfo;

    private boolean mBackgroundScanInProgress = false;
    private boolean mBackgroundScanIsConfigured = false;
    private boolean mIsOpen;

    static public abstract class ChannelBroadcastListener
    {
        public abstract void onBroadcastChanged(ChannelInfo newInfo);  // This is necessary, but I do not understand why
        public abstract void onBackgroundScanStateChange(boolean backgroundScanInProgress, boolean backgroundScanIsConfigured);
        public abstract void onChannelDeath();
    }

    public ChannelController(AntChannel antChannel, ChannelInfo channelInfo,
                             ChannelBroadcastListener broadcastListener, boolean isBackgroundScan)
    {
        mAntChannel = antChannel;
        mChannelInfo = channelInfo;
        mChannelBroadcastListener = broadcastListener;

        if (isBackgroundScan){
            try {
                if(antChannel != null) {
                    // Checking the ANT chip's current background scan state; only one background scan can occur at a time
                    mBackgroundScanInProgress = mAntChannel.getBackgroundScanState().isInProgress();
                    mBackgroundScanIsConfigured = mAntChannel.getBackgroundScanState().isConfigured();
                    mChannelBroadcastListener.onBackgroundScanStateChange(mBackgroundScanInProgress, mBackgroundScanIsConfigured);
                }
            } catch (RemoteException e) {
                Log.e(TAG, "Error communicating with ARS", e);
            }
//            mChannelBroadcastListener.onBackgroundScanStateChange(mBackgroundScanInProgress, mBackgroundScanIsConfigured);
            configureBackgroundScanningChannel();
        }
        else{
            openChannel();
        }

        // Setting the received channel to be a background scanning channel

    }

    public void configureBackgroundScanningChannel()
    {
        // Setting the channel ID to search for any device (i.e. device number
        // is 0)
        ChannelId channelId = new ChannelId(mChannelInfo.deviceNumber, mChannelInfo.deviceType,
                                            mChannelInfo.transmissionType);

        try
        {
            // Setting channel and adapter event handlers
            mAntChannel.setChannelEventHandler(mChannelEventCallback);

            // This adapter receives adapter wide events, such as background scan state changes
            mAntChannel.setAdapterEventHandler(mAdapterEventHandler);

            // To set up a background scan channel, extended assign needs to be
            // used with background scanning set to enabled
            ExtendedAssignment extendedAssign = new ExtendedAssignment();
            extendedAssign.enableBackgroundScanning();

            // Assigning channel as slave (RX channel) with the desired extended
            // assignment features
            mAntChannel.assign(ChannelType.BIDIRECTIONAL_SLAVE, extendedAssign);

            // Setting extended data to be received with each ANT message via
            // Lib Config
            LibConfig libconfig = new LibConfig();
            libconfig.setEnableChannelIdOutput(true);
            mAntChannel.setAdapterWideLibConfig(libconfig);

            // Configuring channel
            mAntChannel.setChannelId(channelId);
            mAntChannel.setPeriod(mChannelInfo.period);
            mAntChannel.setRfFrequency(mChannelInfo.offsetFrequency);

        } catch (RemoteException e) {
            Log.e(TAG, "", e);
        } catch (AntCommandFailedException e) {
            // This will release, and therefore unassign if required
            Log.e(TAG, "", e);
        } catch (UnsupportedFeatureException e) {
            Log.e(TAG, "", e);
        }

    }

    public void openBackgroundScanningChannel() {
        if (null != mAntChannel)
        {
            if (mIsOpen)
            {
                Log.w(TAG, "Channel was already opened");
            }
            else
            {
                try {
                    if (!mBackgroundScanInProgress) {
                        // Opening channel
                        mAntChannel.open();
                        mIsOpen = true;
                        Log.d(TAG, "Opened background scanning channel");
                    }
                } catch (RemoteException e) {
                    Log.e(TAG, "", e);
                } catch (AntCommandFailedException e) {
                    // This will release, and therefore un-assign if required
                    Log.e(TAG, "", e);
                }
            }
        }
        else
        {
            Log.w(TAG, "No channel available");
        }
    }

    boolean openChannel()
    {
        if(null != mAntChannel)
        {
            if(mIsOpen)
            {
                Log.w(TAG, "Channel was already open");
            }
            else
            {
                /*
                 * Although this reference code sets ChannelType to either a transmitting master or a receiving slave,
                 * the standard for ANT is that channels communication is bidirectional. The use of single-direction
                 * communication in this app is for ease of understanding as reference code. For more information and
                 * any additional features on ANT channel communication, refer to the ANT Protocol Doc found at:
                 * http://www.thisisant.com/resources/ant-message-protocol-and-usage/
                 */
                ChannelType channelType = (mChannelInfo.isMaster ?
                        ChannelType.BIDIRECTIONAL_MASTER : ChannelType.BIDIRECTIONAL_SLAVE);
                ;

                // Channel ID message contains device number, type and transmission type. In
                // order for master (TX) channels and slave (RX) channels to connect, they
                // must have the same channel ID, or wildcard (0) is used.
                ChannelId channelId = new ChannelId(mChannelInfo.deviceNumber,
                        mChannelInfo.deviceType, mChannelInfo.transmissionType);

                try
                {
                    // Setting the channel event handler so that we can receive messages from ANT
                    mAntChannel.setChannelEventHandler(mChannelEventCallback);

                    // Performs channel assignment by assigning the type to the channel. Additional
                    // features (such as, background scanning and frequency agility) can be enabled
                    // by passing an ExtendedAssignment object to assign(ChannelType, ExtendedAssignment).
                    mAntChannel.assign(channelType);

                    /*
                     * Configures the channel ID, messaging period and rf frequency after assigning,
                     * then opening the channel.
                     *
                     * For any additional ANT features such as proximity search or background scanning, refer to
                     * the ANT Protocol Doc found at:
                     * http://www.thisisant.com/resources/ant-message-protocol-and-usage/
                     */
                    mAntChannel.setChannelId(channelId);
                    mAntChannel.setPeriod(mChannelInfo.period);
                    mAntChannel.setRfFrequency(mChannelInfo.offsetFrequency);
                    mAntChannel.open();
                    mIsOpen = true;

                    Log.d(TAG, "Opened channel with device number: " + mChannelInfo.deviceNumber);
                } catch (RemoteException e) {
                    channelError(e);
                } catch (AntCommandFailedException e) {
                    // This will release, and therefore unassign if required
                    channelError("Open failed", e);
                }
            }
        }
        else
        {
            Log.w(TAG, "No channel available");
        }

        return mIsOpen;
    }

    private IAntAdapterEventHandler mAdapterEventHandler = new IAntAdapterEventHandler() {

        @Override
        public void onEventBufferSettingsChange(EventBufferSettings newEventBufferSettings) {
            // Not using the event buffer; can ignore these events
        }

        @Override
        // Called whenever the background scan state has changed
        public void onBackgroundScanStateChange(BackgroundScanState newBackgroundScanState) {
            Log.i(TAG, "Received Background scan state change: " + newBackgroundScanState.toString());

            // Applications can use this to determine if it is safe to
            // open a background scan if scan was previously used by other app
            mBackgroundScanInProgress = newBackgroundScanState.isInProgress();
            mBackgroundScanIsConfigured = newBackgroundScanState.isConfigured();

            mChannelBroadcastListener.onBackgroundScanStateChange(mBackgroundScanInProgress, mBackgroundScanIsConfigured);
        }

        @Override
        public void onBurstStateChange(BurstState newBurstSate) {
            // Not bursting; can ignore these events
        }

        @Override
        public void onLibConfigChange(LibConfig newLibConfig) {
            Log.i(TAG, "Received Lib Config change: " + newLibConfig.toString());
        }

    };
    /**
     * Implements the Channel Event Handler Interface so that messages can be
     * received and channel death events can be handled.
     */
    public class ChannelEventCallback implements IAntChannelEventHandler
    {
        private void updateData(byte[] data) {
            mChannelInfo.broadcastData = data;
            mChannelBroadcastListener.onBroadcastChanged(mChannelInfo);
            String dString = toPacketString(data);
            Log.d(TAG, "Full Rx: "+ dString);
        }

        @Override
        public void onChannelDeath()
        {
            // Display channel death message when channel dies
            displayChannelError("Channel Death");
        }

        @Override
        public void onReceiveMessage(MessageFromAntType messageType, AntMessageParcel antParcel) {
//            Log.d(TAG, "Rx: "+ antParcel);

            // Switching on message type to handle different types of messages
            switch(messageType)
            {
                // If data message, construct from parcel and update channel data
                case BROADCAST_DATA:
                    // Rx Data
                    Log.d(TAG, "BROADCAST_DATA: ");
                    updateData(new BroadcastDataMessage(antParcel).getPayload());
                    break;
                case ACKNOWLEDGED_DATA:
                    // Rx Data
                    Log.d(TAG, "ACKNOWLEDGED_DATA: ");
                    updateData(new AcknowledgedDataMessage(antParcel).getPayload());
                    break;
                case CHANNEL_EVENT:
                    // Constructing channel event message from parcel
                    ChannelEventMessage eventMessage = new ChannelEventMessage(antParcel);

                    // Switching on event code to handle the different types of channel events
                    switch(eventMessage.getEventCode())
                    {
                        case TX:
                            // Use old info as this is what remote device has just received
                            mChannelBroadcastListener.onBroadcastChanged(mChannelInfo);

                            if(mIsOpen)
                            {
                                try {
                                    // Setting the data to be broadcast on the next channel period
                                    mAntChannel.setBroadcastData(mChannelInfo.broadcastData);
                                } catch (RemoteException e) {
                                    channelError(e);
                                }
                            }
                            break;
                        case RX_SEARCH_TIMEOUT:
                            // TODO May want to keep searching
                            displayChannelError("No Device Found");
                            break;
                        case CHANNEL_CLOSED:
                        case CHANNEL_COLLISION:
                        case RX_FAIL:
                        case RX_FAIL_GO_TO_SEARCH:
                        case TRANSFER_RX_FAILED:
                        case TRANSFER_TX_COMPLETED:
                        case TRANSFER_TX_FAILED:
                        case TRANSFER_TX_START:
                        case UNKNOWN:
                            // TODO More complex communication will need to handle these events
                            break;
                    }
                    break;
                case ANT_VERSION:
                case BURST_TRANSFER_DATA:
                case CAPABILITIES:
                case CHANNEL_ID:
                case CHANNEL_RESPONSE:
                case CHANNEL_STATUS:
                case SERIAL_NUMBER:
                case OTHER:
                    // TODO More complex communication will need to handle these message types
                    break;
            }
        }
    }

    public ChannelInfo getCurrentInfo()
    {
        return mChannelInfo;
    }

    void displayChannelError(String displayText)
    {
        mChannelInfo.die(displayText);
        mChannelBroadcastListener.onBroadcastChanged(mChannelInfo);
    }

    void channelError(RemoteException e) {
        String logString = "Remote service communication failed.";

        Log.e(TAG, logString);

        displayChannelError(logString);
    }

    void channelError(String error, AntCommandFailedException e) {
        StringBuilder logString;

        if(e.getResponseMessage() != null) {
            String initiatingMessageId = "0x"+ Integer.toHexString(
                    e.getResponseMessage().getInitiatingMessageId());
            String rawResponseCode = "0x"+ Integer.toHexString(
                    e.getResponseMessage().getRawResponseCode());

            logString = new StringBuilder(error)
                    .append(". Command ")
                    .append(initiatingMessageId)
                    .append(" failed with code ")
                    .append(rawResponseCode);
        } else {
            String attemptedMessageId = "0x"+ Integer.toHexString(
                    e.getAttemptedMessageType().getMessageId());
            String failureReason = e.getFailureReason().toString();

            logString = new StringBuilder(error)
                    .append(". Command ")
                    .append(attemptedMessageId)
                    .append(" failed with reason ")
                    .append(failureReason);
        }

        Log.e(TAG, logString.toString());

        mAntChannel.release();

        displayChannelError("ANT Command Failed");
    }

    public void close()
    {
        // TODO kill all our resources
        if (null != mAntChannel)
        {
            mIsOpen = false;

            // Releasing the channel to make it available for others.
            // After releasing, the AntChannel instance cannot be reused.
            mAntChannel.release();
            mAntChannel = null;
        }

        displayChannelError("Channel Closed");
    }

    public static String toPacketString(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();

        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if(i != 0) {
                hexString.append('-');
            }
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
    }
}

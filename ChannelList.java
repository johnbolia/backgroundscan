/*
 * Copyright 2012 Dynastream Innovations Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.dsi.ant.sample.backgroundscan;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ChannelList extends Activity implements ChannelListMin.CallbackInterface
         {
    private static final String TAG = ChannelList.class.getSimpleName();

    // new
    private ChannelListMin clm = new ChannelListMin(this, this);

    private final String PREF_SCAN_STARTED_KEY = "ChannelList.SCAN_STARTED";
    private boolean mScanStarted = false;


    public ArrayList<String> mChannelDisplayList = new ArrayList<String>();
    private ArrayAdapter<String> mChannelListAdapter;
    private SparseArray<Integer> mIdChannelListIndexMap = new SparseArray<Integer>();

    public void onAllowStartScanCallback(final boolean allowStartScan) {
        ((Button) findViewById(R.id.button_Scan)).setEnabled(allowStartScan);
    }


     private void initButtons()
     {
         Log.v(TAG, "initButtons...");

         // Register Start Scan Button handler
         Button button_startService = (Button) findViewById(R.id.button_Service);
         Button button_startScan = (Button) findViewById(R.id.button_Scan);
         Button button_stopScan = (Button) findViewById(R.id.button_StopScan);
         button_startScan.setEnabled(false);

         button_startService.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    clm.setDeviceTypeAndPeriod(111, 400);
                    clm.doBindChannelService();
                }
            });

         button_startScan.setOnClickListener(new OnClickListener()
         {
             @Override
             public void onClick(View v)
             {
                 mChannelDisplayList.clear();
                 mIdChannelListIndexMap.clear();
                 mChannelListAdapter.notifyDataSetChanged();
                 clm.startBackgroundScan();
             }
         });

         button_stopScan.setOnClickListener(new OnClickListener()
         {
             @Override
             public void onClick(View v)
             {
                 stopBackgroundScan();
             }
         });

         Log.v(TAG, "...initButtons");
     }

    private void initPrefs()
    {
        Log.v(TAG, "initPrefs...");

        // Handle resuming the current state of data collection as saved in the
        // preference
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);

        mScanStarted = preferences.getBoolean(PREF_SCAN_STARTED_KEY, false);

        Log.v(TAG, "...initPrefs");
    }

    private void savePrefs()
    {
        Log.v(TAG, "savePrefs...");

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean(PREF_SCAN_STARTED_KEY, mScanStarted);

        editor.commit();

        Log.v(TAG, "...savePrefs");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(TAG, "onCreate...");

        clm.mChannelServiceBound = false;

        setContentView(R.layout.activity_channel_list);
        initPrefs();
        mChannelListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                android.R.id.text1, mChannelDisplayList);
        ListView listView_channelList = (ListView) findViewById(R.id.listView_channelList);
        listView_channelList.setAdapter(mChannelListAdapter);

//        if (!clm.mChannelServiceBound) {
//            clm.doBindChannelService();
//        }

        initButtons();
        mChannelDisplayList.clear();
        mIdChannelListIndexMap.clear();
        mChannelListAdapter.notifyDataSetChanged();
        Log.v(TAG, "...onCreate");
    }

    @Override
    protected void onResume() {
        clm.resume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        clm.pause();
        super.onPause();
    }

    @Override
    public void onDestroy()
    {
        clm.destroy();
        super.onDestroy();
    }

    public void refreshList()
    {
        Log.v(TAG, "refreshList...");

        if (null != clm.mChannelService)
        {
            ArrayList<ChannelInfo> chInfoList = clm.mChannelService
                    .getCurrentChannelInfoForAllChannels();
            mChannelDisplayList.clear();
            for (ChannelInfo i : chInfoList)
            {
                addChannelToList(i);
            }
            mChannelListAdapter.notifyDataSetChanged();
        }
        Log.v(TAG, "...refreshList");
    }

    public void externalLogger(String logString){
        Log.i(TAG, "' externalLogger:" + logString);
    }

    private void addChannelToList(ChannelInfo channelInfo)
    {
        Log.v(TAG, "addChannelToList...");
        mIdChannelListIndexMap.put(channelInfo.deviceNumber, mChannelDisplayList.size());
        Log.v(TAG, "...addChannelToList");
    }

     public void channelChange(final ChannelInfo newInfo)
     {
         final Integer index = mIdChannelListIndexMap.get(newInfo.deviceNumber);
         if (index == null) {
             addChannelToList(newInfo);
             runOnUiThread(new Runnable()
             {
                 @Override
                 public void run()
                 {
                     mChannelDisplayList.add(getDisplayText(newInfo));
                     mChannelListAdapter.notifyDataSetChanged();
                 }
             });
         } else {
             runOnUiThread(new Runnable()
             {
                 @Override
                 public void run()
                 {
                     mChannelDisplayList.set(index.intValue(), getDisplayText(newInfo));
                     mChannelListAdapter.notifyDataSetChanged();
                 }
             });
         }
     }

    private static String getDisplayText(ChannelInfo channelInfo)
    {
        Log.v(TAG, "getDisplayText...");
        String displayText;
        if (channelInfo.error)
        {
            displayText = String.format("#%-6d !:%s", channelInfo.deviceNumber,
                    channelInfo.getErrorString());
        }
        else
        {
            if (channelInfo.isMaster)
            {
                displayText = String.format("#%-6d Tx:[%2d]", channelInfo.deviceNumber,
                        channelInfo.broadcastData[0] & 0xFF);
            }
            else
            {
                displayText = String.format("#%-6d Rx:[%16d]", channelInfo.deviceNumber,
                        channelInfo.broadcastData[7] & 0xFF);
            }
        }
        Log.v(TAG, "...getDisplayText");
        return displayText;
    }

    private void stopBackgroundScan()
    {
        Log.v(TAG, "clearAllChannels...");
        if (null != clm.mChannelService)
        {
            clm.mChannelService.stopBackgroundScan();
            mChannelDisplayList.clear();
            mIdChannelListIndexMap.clear();
            mChannelListAdapter.notifyDataSetChanged();
        }
        Log.v(TAG, "...clearAllChannels");
    }
}

package com.dsi.ant.sample.backgroundscan;


import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.dsi.ant.channel.ChannelNotAvailableException;

public class ChannelListMin {
    private static final String TAG = ChannelList.class.getSimpleName();

    Activity mActivity;
    public ChannelService.ChannelServiceComm mChannelService = null;
    public boolean mChannelServiceBound = false;
    int deviceType = 46;  // 46 = Aerodynamic Device
    int channelPeriod = 8192;  // 4 Hz

    interface CallbackInterface
    {
        void onAllowStartScanCallback(final boolean allowStartScan);
        void channelChange(ChannelInfo channelInfo);
        void refreshList();
        void externalLogger(String logString);
    }

    CallbackInterface mCallbackInterface;

    public ChannelListMin(Activity activity_local, CallbackInterface foreignCallback){
        mActivity = activity_local;
        mCallbackInterface = foreignCallback;
    }

    public void setDeviceTypeAndPeriod(int lDeviceType, int lChannelPeriod){
        deviceType = lDeviceType;
        channelPeriod = lChannelPeriod;
    }

    public void doBindChannelService()
    {
        Log.i(TAG, "clm doBindChannelService...");
        Intent bindIntent = new Intent(mActivity, ChannelService.class);
        Log.i(TAG, "got here (intent)...");
        mActivity.startService(bindIntent);
        Log.i(TAG, ".... and got here...");
        mChannelServiceBound = mActivity.bindService(bindIntent, mChannelServiceConnection,
                Context.BIND_AUTO_CREATE);
        if (!mChannelServiceBound) // If the bind returns false, run the unbind
            doUnbindChannelService();
        Log.i(TAG, "  Channel Service binding = " + mChannelServiceBound);
        Log.i(TAG, "...doBindChannelService");
    }

    public void doUnbindChannelService()
    {
        Log.v(TAG, "doUnbindChannelService...");
        if (mChannelServiceBound)
        {
            mActivity.unbindService(mChannelServiceConnection);
            mChannelServiceBound = false;
        }
        Log.v(TAG, "...doUnbindChannelService");
    }

    public void resume() {
        // if null then will be set in onServiceConnected()
        if(mChannelService != null) {
            mChannelService.setActivityIsRunning(true);
        }
    }

    public void pause() {
        if(mChannelService != null) {
            mChannelService.setActivityIsRunning(false);
        }
    }

    public void destroy()
    {
        Log.v(TAG, "onDestroy...");
        doUnbindChannelService();
        if (mActivity.isFinishing())
        {
            mActivity.stopService(new Intent(mActivity, ChannelService.class));
        }
        mChannelServiceConnection = null;
        Log.v(TAG, "...onDestroy");
    }

    public ServiceConnection mChannelServiceConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder)
        {
            Log.v(TAG, "mChannelServiceConnection.onServiceConnected...");
            Log.v(TAG, "/n/n/n got here !!!!!! /n/n/n/n/n/n");
            mChannelService = (ChannelService.ChannelServiceComm) serviceBinder;
            mChannelService.setChannelInfo(deviceType, channelPeriod);
            mChannelService.setOnChannelChangedListener(new ChannelService.ChannelChangedListener()
            {
                @Override
                public void onChannelChanged(final ChannelInfo newInfo)
                {
                    mCallbackInterface.channelChange(newInfo);
                }

                @Override
                public void onAllowStartScan(final boolean allowStartScan) {
                    mActivity.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                          mCallbackInterface.onAllowStartScanCallback(allowStartScan);
                        }
                    });
                }
            });

            mCallbackInterface.refreshList();
            mChannelService.setActivityIsRunning(true);
            Log.v(TAG, "...mChannelServiceConnection.onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0)
        {
            Log.v(TAG, "mChannelServiceConnection.onServiceDisconnected...");
            mChannelService = null;
            Log.v(TAG, "...mChannelServiceConnection.onServiceDisconnected");
        }
    };

    public void startBackgroundScan()
    {
        Log.v(TAG, "startBackgroundScan...");

        if (null != mChannelService)
        {
            try
            {
                mChannelService.openBackgroundScanChannel();
            } catch (ChannelNotAvailableException e)
            {
                Toast.makeText(mActivity, "Channel Not Available", Toast.LENGTH_SHORT).show();
            }
        }

        Log.v(TAG, "...startBackgroundScan");
    }
}
